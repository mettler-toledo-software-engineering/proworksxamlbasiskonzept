﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace mt.proxaml.aprprinter
{
    public interface IApr331
    {
        event EventHandler<PrintResult> PrintExecuted;

        Task<bool> Print(List<string> template);
    }
}