﻿using log4net;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Communication;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using MT.Singularity.IO;
using MT.Singularity.Serialization;

namespace mt.proxaml.aprprinter
{
    public class Apr331 : IApr331
    {
        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(Apr331);
        private readonly StringSerializer _stringSerializer;
        private const int TimeDelayBetweenPrint = 100;
        public event EventHandler<PrintResult> PrintExecuted;


        public Apr331(StringSerializer stringSerializer)
        {
            _stringSerializer = stringSerializer;
        }

        public async Task<bool> Print(List<string> template)
        {

            try
            {
                await _stringSerializer.OpenAsync();
                await WriteBufferToChannel(template);

                await _stringSerializer.CloseAsync();
                PrintExecuted?.Invoke(this, PrintResult.PrintOk);

                return true;
            }
            catch (Exception e)
            {
                Logger.ErrorEx("printing exception", SourceClass, e);
                PrintExecuted?.Invoke(this, PrintResult.SerialChannelException);
                return false;
            }
        }



        private async Task WriteBufferToChannel(List<string> template)
        {
            foreach (string line in template)
            {
                await _stringSerializer.WriteAsync($"{line}");
                Thread.Sleep(TimeDelayBetweenPrint);
            }
        }
    }
}
