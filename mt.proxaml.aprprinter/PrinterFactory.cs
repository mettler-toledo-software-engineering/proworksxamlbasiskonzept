﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using MT.Singularity.IO;
using MT.Singularity.Platform.Devices;
using MT.Singularity.Serialization;

namespace mt.proxaml.aprprinter
{
    public class PrinterFactory : IPrinterFactory
    {
        private readonly IInterfaceService _interfaceService;
        public PrinterFactory(IInterfaceService interfaceService)
        {
            _interfaceService = interfaceService;
        }

        public async Task<Apr331> CreateApr331Printer(Resistance resistance, Contrast contrast, int printerPort)
        {
            var serializer = await SetupConnection(printerPort);
            return new Apr331(serializer);
        }
        public async Task<Apr331> CreateApr331Printer(Resistance resistance, int printerPort)
        {
            Contrast contrast = Contrast.K4;
            var serializer = await SetupConnection(printerPort);
            return new Apr331(serializer);
        }
        public async Task<IApr331> CreateApr331Printer(int printerPort)
        {
            Contrast contrast = Contrast.K4;
            Resistance resistance = Resistance.Class2;
            var serializer = await SetupConnection(printerPort);
            return new Apr331(serializer);
        }

        private async Task<StringSerializer> SetupConnection(int printerport)
        {
            try
            {
                var serialInterfaces = await _interfaceService.GetAllInterfacesOfTypeAsync<ISerialInterface>();
                var serialInterface = serialInterfaces.FirstOrDefault(serial => serial.LogicalPort == printerport);
                var actualConnectionChannel = await serialInterface.CreateConnectionChannelAsync();
                var delimiterSerial = new DelimiterSerializer(actualConnectionChannel, CommonDataSegments.Newline);
                var stringSerializer = new StringSerializer(delimiterSerial);
                return stringSerializer;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                
            }

            return null;

        }
    }
}
