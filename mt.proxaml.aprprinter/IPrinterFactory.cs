﻿using System.Threading.Tasks;

namespace mt.proxaml.aprprinter
{
    public interface IPrinterFactory
    {
        Task<IApr331> CreateApr331Printer(int printerPort);
        Task<Apr331> CreateApr331Printer(Resistance resistance, Contrast contrast, int printerPort);
        Task<Apr331> CreateApr331Printer(Resistance resistance, int printerPort);
    }
}