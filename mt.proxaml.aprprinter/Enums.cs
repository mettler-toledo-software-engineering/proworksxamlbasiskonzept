﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mt.proxaml.aprprinter
{

    public enum PrintResult
    {
        PrintOk,
        SerialChannelException

    }

    public enum Resistance
    {
        Class0,
        Class1,
        Class2,
        Class3,
        Class4,
    }

    public enum Contrast
    {
        K0,
        K1,
        K2,
        K3,
        K4,
        K5,
        K6,
        K7,
        K8
    }

    public static class EnumConverter
    {
        public static string ConvertContrastToString(this Contrast contrast)
        {
            switch (contrast)
            {
                case Contrast.K0: return "K0";
                case Contrast.K1: return "K1";
                case Contrast.K2: return "K2";
                case Contrast.K3: return "K3";
                case Contrast.K4: return "K4";
                case Contrast.K5: return "K5";
                case Contrast.K6: return "K6";
                case Contrast.K7: return "K7";
                case Contrast.K8: return "K8";
                default: return "K4";
            }
        }

        public static string ConvertResistanceToString(this Resistance resistance)
        {
            switch (resistance)
            {
                case Resistance.Class0: return "0";
                case Resistance.Class1: return "1";
                case Resistance.Class2: return "2";
                case Resistance.Class3: return "3";
                case Resistance.Class4: return "4";
                default: return "4";
            }
        }
    }


}
