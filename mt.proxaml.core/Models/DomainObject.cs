﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mt.proxaml.core.Models
{
    /// <summary>
    /// Base Object for all Models with database representation
    /// </summary>
    public class DomainObject
    {
        public int Id { get; set; }
    }
}
