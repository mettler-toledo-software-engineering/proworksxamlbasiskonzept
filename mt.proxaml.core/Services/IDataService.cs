﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace mt.proxaml.core.Services
{
    /// <summary>
    /// Generic Base CRUD interface with concret implementations in application layer
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IDataService<T>
    {
        Task<IEnumerable<T>> GetAll();

        Task<T> Get(int id);

        Task<T> Create(T entity);

        Task<T> Update(int id, T entity);

        Task<bool> Delete(int id);
    }
}
