﻿using mt.proxaml.core.Models;
using mt.proxaml.core.Services;
using System;
using System.Collections.Generic;
using System.Text;
using mt.proxaml.persistence.Context;
using mt.proxaml.persistence.Services.Common;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace mt.proxaml.persistence.Services
{
    /// <summary>
    /// Generic Database Access for all simple CRUD Operations
    /// can be extended by individual Dataservices with more specific implementations
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class GenericDataService<T> : IDataService<T> where T : DomainObject
    {
        protected readonly TestDbContextFactory ContextFactory;
        protected readonly NonQueryDataService<T> NonQueryDataService;

        public GenericDataService(TestDbContextFactory contextFactory)
        {
            ContextFactory = contextFactory;
            NonQueryDataService = new NonQueryDataService<T>(contextFactory);
        }

        public async Task<T> Create(T entity)
        {
            return await NonQueryDataService.Create(entity);
        }

        public async Task<bool> Delete(int id)
        {
            return await NonQueryDataService.Delete(id);
        }
        public async Task<T> Update(int id, T entity)
        {
            return await NonQueryDataService.Update(id, entity);
        }

        public async Task<T> Get(int id)
        {
            using (TestDbContext context = ContextFactory.CreateDbContext())
            {
                T entity = await context.Set<T>().FirstOrDefaultAsync(e => e.Id == id);
                return entity;
            }
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            using (TestDbContext context = ContextFactory.CreateDbContext())
            {
                IEnumerable<T> entities = await context.Set<T>().ToListAsync();
                return entities;
            }
        }

    }
}
