﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using mt.proxaml.application.Models;
using mt.proxaml.application.Services.DataAccess;
using mt.proxaml.persistence.Context;

namespace mt.proxaml.persistence.Services
{

    /// <summary>
    /// specific dataservice for transaction that are not covered by the generic dataservice
    /// </summary>
    public class TransactionDataService : GenericDataService<Transaction>, ITransactionService
    {
        public TransactionDataService(TestDbContextFactory contextFactory) : base(contextFactory)
        {
        }
        /// <summary>
        /// access to the database
        /// </summary>
        /// <returns>the newest transaction</returns>
        public async Task<Transaction> GetNewestTransaction()
        {
            using (TestDbContext context = ContextFactory.CreateDbContext())
            {
                var transaction = await context.Transactions.OrderBy(t => t.TimeStamp).FirstOrDefaultAsync();

                return transaction;
            }
        }
    }
}
