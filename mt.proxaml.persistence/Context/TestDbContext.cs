﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using mt.proxaml.application.Models;

namespace mt.proxaml.persistence.Context
{
    /// <summary>
    /// Here your Database tables need to be Defined as DbSets
    /// Rename this context to match it with your Database Name
    /// </summary>
    public class TestDbContext : DbContext
    {
        public DbSet<Transaction> Transactions { get; set; }

        public TestDbContext(DbContextOptions options) : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
