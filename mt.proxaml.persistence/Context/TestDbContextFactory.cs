﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace mt.proxaml.persistence.Context
{
    // this project uses .net standard 2.0 to be compatible with .net 4.6.1
    // nuget for Microsoft.EntityFrameworkCore, Microsoft.EntityFrameworkCore.Sqlite, Microsoft.EntityFrameworkCore.SqlServer, Microsoft.EntityFrameworkCore.Tools in version 3.X
    /// <summary>
    /// a factory for db context, to make sure every database access uses the correct context ( connection string / db access type)
    /// can be configured in app.cs
    /// </summary>
    public class TestDbContextFactory
    {
        public Action<DbContextOptionsBuilder> ConigerDbContext { get; set; }

        public TestDbContextFactory(Action<DbContextOptionsBuilder> conigerDbContext)
        {
            ConigerDbContext = conigerDbContext;

        }

        public TestDbContext CreateDbContext()
        {
            DbContextOptionsBuilder<TestDbContext> options = new DbContextOptionsBuilder<TestDbContext>();
            ConigerDbContext(options);

            return new TestDbContext(options.Options);
        }

    }
}
