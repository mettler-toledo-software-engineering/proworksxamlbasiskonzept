﻿namespace mt.proxaml.application.Services.SystemUpdate
{
    public interface ISystemUpdateService
    {
        bool ExecuteUpdate();
    }
}