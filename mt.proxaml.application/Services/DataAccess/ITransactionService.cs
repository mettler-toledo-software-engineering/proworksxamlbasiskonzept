﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.proxaml.application.Models;
using mt.proxaml.core.Services;

namespace mt.proxaml.application.Services.DataAccess
{
    /// <summary>
    /// concrete implementation of generic IDataservice for Transaction access
    /// </summary>
    public interface ITransactionService : IDataService<Transaction>
    {
        Task<Transaction> GetNewestTransaction();
    }
}
