﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.proxaml.core.Models;

namespace mt.proxaml.application.Models
{
    /// <summary>
    /// example model for a transaction, implements the domain object in order to receive an Id for DB access
    /// </summary>
    public class Transaction : DomainObject
    {
        public DateTime TimeStamp { get; set; }
    }
}
