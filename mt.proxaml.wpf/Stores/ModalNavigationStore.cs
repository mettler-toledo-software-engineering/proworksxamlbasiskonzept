﻿using mt.proxaml.wpf.ViewModels;
using System;
using MT.Singularity.Composition;

namespace mt.proxaml.wpf.Stores
{
    /// <summary>
    /// handles the navigation for the current modal that needs to be displayed
    /// </summary>
    [Export(typeof(ModalNavigationStore))]
    [InjectionBehavior(IsSingleton = true)]
    public class ModalNavigationStore
    {
        private SimpleViewModelBase _currentViewModel;
        /// <summary>
        /// disposes the old viewmodel before setting the new one during navigation, in order to free all eventhandlers
        /// </summary>
        public SimpleViewModelBase CurrentViewModel
        {
            get => _currentViewModel;
            set
            {
                _currentViewModel?.Dispose();
                _currentViewModel = value;
                OnCurrentViewModelChanged();
            }
        }

        public bool ModalIsOpen => CurrentViewModel != null;

        public event Action CurrentViewModelChanged;

        /// <summary>
        /// sets the CurrentViewModel to Null in order to Close the Modal
        /// </summary>
        public void Close()
        {
            CurrentViewModel = null;
        }
        private void OnCurrentViewModelChanged()
        {
            CurrentViewModelChanged?.Invoke();
        }
    }
}
