﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using mt.proxaml.wpf.SetupNodes.MainNode;
using MT.Singularity.Composition;

namespace mt.proxaml.wpf.Stores
{

    /// <summary>
    /// this store provides the current setup configuration of the terminal and triggers events if the setup changes
    /// </summary>
    [InjectionBehavior(IsSingleton = true)]
    public class CustomSetupStore
    {
        public event Action<string> CurrentCustomSetupConfigurationChanged;
        public CustomSetupConfiguration CurrentCustomSetupConfiguration { get; private set; }
        public CustomSetupStore(ICustomSetupComponents customSetupComponents)
        {
            customSetupComponents.GetConfigurationAsync().ContinueWith(HandleException);
        }

        private void HandleException(Task<CustomSetupConfiguration> task)
        {
            if (task.Exception == null)
            {
                CurrentCustomSetupConfiguration = task.Result;
                CurrentCustomSetupConfiguration.PropertyChanged += CurrentCustomSetupConfigurationOnPropertyChanged;
            }
        }

        private void CurrentCustomSetupConfigurationOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            
            CurrentCustomSetupConfigurationChanged?.Invoke(e.PropertyName);
        }
    }
}
