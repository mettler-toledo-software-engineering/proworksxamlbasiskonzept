﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using mt.proxaml.application.Models;
using mt.proxaml.application.Services.DataAccess;


namespace mt.proxaml.wpf.Stores
{
    /// <summary>
    /// an example store which loads data provided by a service
    /// this transaction List is the single source of truth for all application components that are interested in this data
    /// </summary>
    public class TransactionStore : IDataStore
    {
        private readonly List<Transaction> _transactions;
        private readonly ITransactionService _transactionService;


        private Lazy<Task> _loadTransactionsLazy;

        public IEnumerable<Transaction> Transactions => _transactions;

        public event Action TransactionsLoaded;
        public event Action<Transaction> TransactionCreated;
        public event Action<Transaction> CurrentTransactionChanged;
        public event Action TransactionDeleted; 
        private Transaction _currentTransaction;

        public TransactionStore(ITransactionService transactionService)
        {
            _transactions = new List<Transaction>();
            _transactionService = transactionService;
            _loadTransactionsLazy = CreateTransactionsLazy();

        }
        public Transaction CurrentTransaction
        {
            get => _currentTransaction;
            set
            {
                _currentTransaction = value;
                CurrentTransactionChanged?.Invoke(_currentTransaction);
            }
        }

        public async Task UpdateCurrentTransaction(Transaction transaction)
        {
            var result = await _transactionService.Update(transaction.Id, transaction);
            CurrentTransaction = result;
            await RefreshTransactions();
        }
        
        private Lazy<Task> CreateTransactionsLazy()
        {
            return new Lazy<Task>(() => InitializeTransactions());
        }

        private async Task InitializeTransactions()
        {

            IEnumerable<Transaction> transactions = await _transactionService.GetAll();
            _transactions.Clear();
            _transactions.AddRange(transactions);

        }

        public async Task CreateTransaction(Transaction transaction)
        {
            var result = await _transactionService.Create(transaction);
            _transactions.Add(result);
            TransactionCreated?.Invoke(result);
        }

        public async Task<bool> DeleteTransaction(Transaction transaction)
        {

            var success = await _transactionService.Delete(transaction.Id);
            if (success == true)
            {
                await RefreshTransactions();
                CurrentTransaction = null;
                _transactions.RemoveAll(t => t.Id == transaction.Id);
                TransactionDeleted?.Invoke();
            }

            return success;

        }

        public async Task LoadData()
        {
            await _loadTransactionsLazy.Value;
            TransactionsLoaded?.Invoke();
        }

        public async Task RefreshTransactions()
        {
            _loadTransactionsLazy = CreateTransactionsLazy();
            await LoadData();
        }
    }
}
