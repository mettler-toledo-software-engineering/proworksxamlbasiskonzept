﻿using System;
using log4net;
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Messaging;

namespace mt.proxaml.wpf.Stores
{
    /// <summary>
    /// handling of all messages that need to be displayed to the user
    /// IEnginginemessageService can be used to add the message to the info area of singularity as well
    /// </summary>
    [Export(typeof(GlobalMessageStore))]
    [InjectionBehavior(IsSingleton = true)]
    public class GlobalMessageStore
    {
        private static readonly ILog _logger = Log4NetManager.ApplicationLogger;
        private readonly IEngineMessageService _messageService;

        public enum MessageType
        {
            Success,
            Info,
            Error,
            None
        }

        public GlobalMessageStore(IEngineMessageService messageService)
        {
            CurrentMessage = "Applikation Startet...";
            CurrentMessageType = MessageType.Info;
            _messageService = messageService;
        }

        public int MessageDuration { get; private set; } = 5000;

        public string CurrentMessage { get; private set; }

        public event Action CurrentMessageChanged;

        public MessageType CurrentMessageType { get; private set; }

        public void UpdateMessage(string message, MessageType messageType, Exception exception = null)
        {
            MessageDuration = 5000;

            Update(message, messageType, exception);
        }
        /// <summary>
        /// Create a new message in the Message area with message duration in seconds
        /// Error Messages und exceptions will be logged if provided
        /// </summary>
        /// <param name="message"></param>
        /// <param name="messageType"></param>
        /// <param name="messageDuration"> in seconds</param>
        /// <param name="exception"></param>
        public void UpdateMessage(string message, MessageType messageType, int messageDuration, Exception exception = null)
        {
            MessageDuration = messageDuration * 1000;

            Update(message, messageType, exception);

        }

        private void Update(string message, MessageType messageType, Exception exception = null)
        {
            CurrentMessage = message;
            CurrentMessageType = messageType;
            CurrentMessageChanged?.Invoke();

            if (CurrentMessageType == MessageType.Error)
            {
                _logger.Error(message, exception);
            }

            if (CurrentMessageType == MessageType.Info)
            {
                _logger.Info(message);
            }
        }
    }
}
