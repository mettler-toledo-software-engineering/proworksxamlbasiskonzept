﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mt.proxaml.wpf.Stores
{
    /// <summary>
    /// example for a viewmodel generalization, where different viewmodels might need a command to load Data
    /// </summary>
    public interface IDataStore
    {
        Task LoadData();
    }
}
