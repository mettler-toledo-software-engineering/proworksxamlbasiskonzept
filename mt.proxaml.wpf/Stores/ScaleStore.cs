﻿using System;
using System.Threading.Tasks;
using System.Windows;
using mt.proxaml.wpf.Infrastructure;
using MT.Singularity.Composition;
using MT.Singularity.Platform.Application.Value;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Platform.ValueSystem;

namespace mt.proxaml.wpf.Stores
{

    /// <summary>
    /// handles the scale communication and provides methods for commands
    /// can be expanded if more methods are necessary
    /// </summary>
    [InjectionBehavior(IsSingleton = true)]
    public class ScaleStore
    {
        private readonly IScaleService _scaleService;
        private readonly IApplicationValueService _applicationValueService;
        private readonly GlobalMessageStore _messageStore;
        private readonly LanguageStore _languageStore;

        public IScaleService ScaleService => _scaleService;


        public event Action<WeightInformation> WeightChanged;
        private ResourceDictionary _dictionary;
        public IScale SelectedScale { get; private set; }

        public ScaleStore(IScaleService scaleService, IApplicationValueService applicationValueService, GlobalMessageStore messageStore, LanguageStore languageStore)
        {
            _scaleService = scaleService;
            _applicationValueService = applicationValueService;
            _messageStore = messageStore;
            _languageStore = languageStore;
            _applicationValueService.GetApplicationValueObservableAsync().ContinueWith(HandleException);
            _scaleService.SelectedScaleChanged += ScaleServiceOnSelectedScaleChanged;
            _languageStore.LanguageChanged += LanguageStoreOnLanguageChanged;
            _dictionary = _languageStore.CurrentLanguageDictionary;

        }

        private void LanguageStoreOnLanguageChanged()
        {
            _dictionary = _languageStore.CurrentLanguageDictionary;
        }

        private void ScaleServiceOnSelectedScaleChanged()
        {
            SelectedScale = _scaleService.SelectedScale;
        }

        private void HandleException(Task<IObservable<ApplicationValue>> task)
        {
            if (task.Exception != null)
            {
                string text = (string)_dictionary[Globals.ERR_SCALE1];

                _messageStore.UpdateMessage(text, GlobalMessageStore.MessageType.Error, task.Exception);
                return;
            }

            task.Result.Subscribe(ApplicationValueGenerator);
        }

        private void ApplicationValueGenerator(ApplicationValue applicationValue)
        {
            WeightChanged?.Invoke(applicationValue.WeightInformation);
        }


        public Task<WeightInformation> GetStableWeight(UnitType unitType, int timeout) => SelectedScale.GetStableWeightAsync(unitType, timeout);
        public Task<PresetTareResult> PresetTareScale(string tarevalue, KnownUnit unit) => SelectedScale.PresetTareWithResultAsync(tarevalue, unit);

        //the following methods are already handled by default buttons, but can be included if complex scale handling is necessary
        public Task<WeightState> TareScale() => SelectedScale.TareAsync();

        public Task<WeightState> ClearTare() => SelectedScale.ClearTareAsync();

        

        public Task<WeightState> ZeroScale() => SelectedScale.ZeroAsync();
        public Task<bool> SwitchScale() => _scaleService.SwitchSelectedScaleAsync();


    }
}
