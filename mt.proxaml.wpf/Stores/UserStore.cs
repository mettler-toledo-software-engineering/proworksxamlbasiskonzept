﻿using MT.Singularity.Composition;
using System;
using MT.Singularity.Platform.UserManagement;

namespace mt.proxaml.wpf.Stores
{
    /// <summary>
    /// an Example on how to manage permissions and currently logged in users.
    /// </summary>
    [Export(typeof(UserStore))]
    [InjectionBehavior(IsSingleton = true)]
    public class UserStore
    {
        private readonly ISecurityService _securityService;

        public User CurrentUser { get; private set; }
        public event Action CurrentUserChanged;
        
        public UserStore(ISecurityService securityService)
        {
            _securityService = securityService;
            _securityService.CurrentUserChanged += SecurityServiceOnCurrentUserChanged;
        }

        private void SecurityServiceOnCurrentUserChanged(User user)
        {
            CurrentUser = user;
            CurrentUserChanged?.Invoke();
        }

    }
}
