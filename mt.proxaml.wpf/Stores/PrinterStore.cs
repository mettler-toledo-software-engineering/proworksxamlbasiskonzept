﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.proxaml.aprprinter;
using MT.Singularity.Composition;

namespace mt.proxaml.wpf.Stores
{
    /// <summary>
    /// an example on how to implement a apr331 printer with dependency injection
    /// the print method only needs to be called by dedicated command
    /// </summary>
    [InjectionBehavior(IsSingleton = true)]
    public class PrinterStore
    {
        private readonly CustomSetupStore _customSetupStore;
        private readonly IPrinterFactory _printerFactory;
        private IApr331 _apr331Printer;

        public PrinterStore(CustomSetupStore customSetupStore, IPrinterFactory printerFactory)
        {
            _customSetupStore = customSetupStore;
            _customSetupStore.CurrentCustomSetupConfigurationChanged += CustomSetupStoreOnCurrentCustomSetupConfigurationChanged;
            _printerFactory = printerFactory;
            _printerFactory.CreateApr331Printer(customSetupStore.CurrentCustomSetupConfiguration.PrinterPort).ContinueWith(HandleException);
        }

        private void HandleException(Task<IApr331> task)
        {
            if (task.Exception != null)
            {
                Debug.WriteLine("printer could not be initialized");
            }

            _apr331Printer = task.Result;
        }

        private void CustomSetupStoreOnCurrentCustomSetupConfigurationChanged(string propertyname)
        {
            if (propertyname == "PrinterPort")
            {
                _printerFactory.CreateApr331Printer(_customSetupStore.CurrentCustomSetupConfiguration.PrinterPort).ContinueWith(HandleException);
            }
            
        }

        public Task<bool> Print(List<string> printTemplate)
        {
            return _apr331Printer.Print(new List<string>(){"test","test2"});
        }
    }
}
