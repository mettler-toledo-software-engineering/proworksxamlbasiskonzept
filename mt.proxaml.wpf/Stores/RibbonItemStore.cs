﻿using mt.proxaml.wpf.Infrastructure;
using MT.Singularity.Platform.UI.Component.Ribbon;
using MT.Singularity.Platform.UI.Shell.Ribbon;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MT.Singularity.Composition;
using System.Collections.ObjectModel;
using mt.proxaml.wpf.CustomItems;

namespace mt.proxaml.wpf.Stores
{

    /// <summary>
    /// holds all default buttons so they can be called and referenced by all viewmodels
    /// </summary>
    [InjectionBehavior(IsSingleton = true)]
    //[Export(typeof(RibbonItemStore))]
    public class RibbonItemStore
    {
        private readonly RibbonItemsProvider _ribbonItemsProvider;

        public RibbonItemStore(RibbonItemsProvider ribbonItemsProvider)
        {
            _ribbonItemsProvider = ribbonItemsProvider;

            CreateRibbonItems().ContinueWith(HandleException);
            
        }

        public RibbonItem ZeroKey { get; private set; }
        public RibbonItem Tarekey { get; private set; }
        public RibbonItem Clearkey { get; private set; }
        public RibbonItem Printkey { get; private set; }
        public RibbonItem PresetTarekey { get; private set; }
        public RibbonItem SwitchScalekey { get; private set; }
        public RibbonItem SwitchUnitkey { get; private set; }
        public RibbonItem Alibikey { get; private set; }
        public RibbonItem Highreskey { get; private set; }

        public RibbonItem PlaceHolderKey { get; } = new RibbonItem(Globals.PlaceHolderKeyId);

        private void HandleException(Task task)
        {
            if (task.Exception != null)
            {
                throw new Exception();
            }

        }

        private async Task CreateRibbonItems()
        {
            ZeroKey = await _ribbonItemsProvider.CreateItemAsync(Globals.ZeroItemId) as RibbonItem;
            Tarekey = await _ribbonItemsProvider.CreateItemAsync(Globals.TareItemId) as RibbonItem;
            Clearkey = await _ribbonItemsProvider.CreateItemAsync(Globals.ClearItemId) as RibbonItem;

            Printkey = await _ribbonItemsProvider.CreateItemAsync(Globals.PrintItemId) as RibbonItem;
            PresetTarekey = await _ribbonItemsProvider.CreateItemAsync(Globals.PresetTareItemId) as RibbonItem;
            SwitchScalekey = await _ribbonItemsProvider.CreateItemAsync(Globals.SwitchScaleItemId) as RibbonItem;

            SwitchUnitkey = await _ribbonItemsProvider.CreateItemAsync(Globals.SwitchUnitItemId) as RibbonItem;
            Alibikey = await _ribbonItemsProvider.CreateItemAsync(Globals.AlibiTableItemId) as RibbonItem;
            Highreskey = await _ribbonItemsProvider.CreateItemAsync(Globals.ShowHighResolutionItemId) as RibbonItem;

        }

    }
}
