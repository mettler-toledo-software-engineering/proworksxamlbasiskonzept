﻿using MT.Singularity.Localization;
using System;
using System.Windows;
using MT.Singularity.Composition;

namespace mt.proxaml.wpf.Stores
{
    /// <summary>
    /// handles language changes and sets the necessary dictionary when the language changes
    /// provides events for viewmodels to update their language dependent properties
    /// </summary>
    [Export(typeof(LanguageStore))]
    [InjectionBehavior(IsSingleton = true)]
    public class LanguageStore
    {
        private readonly ResourceDictionary _englishDictionary = new ResourceDictionary{ Source = new Uri(@"..\Languages\LanguageEN.xaml", UriKind.Relative) };
        private readonly ResourceDictionary _frenchDictionary = new ResourceDictionary { Source = new Uri(@"..\Languages\LanguageFR.xaml", UriKind.Relative) };
        private readonly ResourceDictionary _italianDictionary = new ResourceDictionary { Source = new Uri(@"..\Languages\LanguageIT.xaml", UriKind.Relative) };
        private readonly ResourceDictionary _germanDictionary = new ResourceDictionary { Source = new Uri(@"..\Languages\LanguageDE.xaml", UriKind.Relative) };
        private readonly ResourceDictionary _spanishDictionary = new ResourceDictionary { Source = new Uri(@"..\Languages\LanguageES.xaml", UriKind.Relative) };

        public event Action LanguageChanged;
        public LanguageStore()
        {
            LocalizationManager.Instance.LanguageChanged += Instance_LanguageChanged;
            SetLanguage();
        }

        private void Instance_LanguageChanged(object sender, EventArgs e)
        {
            SetLanguage();
        }

        private void SetLanguage()
        {
            //remove existing language dictionary from application resources
            if (CurrentLanguageDictionary != null)
                Application.Current.Resources.MergedDictionaries.Remove(CurrentLanguageDictionary);

            switch (LocalizationManager.Instance.CurrentLanguage)
            {
                case "en-EN":
                case "en-US":
                    CurrentLanguageDictionary = _englishDictionary;
                    break;
                case "it":
                    CurrentLanguageDictionary = _italianDictionary;
                    break;
                case "fr-FR":
                    CurrentLanguageDictionary = _frenchDictionary;
                    break;
                case "es-ES":
                    CurrentLanguageDictionary = _spanishDictionary;
                    break;
                case "de-DE":
                default:
                    CurrentLanguageDictionary = _germanDictionary;
                    break;
            }
            //add new language dictionary to application resources
            Application.Current.Resources.MergedDictionaries.Add(CurrentLanguageDictionary);
            LanguageChanged?.Invoke();
        }

        public ResourceDictionary CurrentLanguageDictionary { get; private set; }
    }
}
