﻿using MT.Singularity.Composition;
using System;
using mt.proxaml.wpf.ViewModels;

namespace mt.proxaml.wpf.Stores
{
    /// <summary>
    /// handles the current viewmodel that needs to be displayed and informs the homescreen viewmodel if navigation happens
    /// </summary>
    [Export(typeof(NavigationStore))]
    [InjectionBehavior(IsSingleton = true)]
    public class NavigationStore
    {
        private SimpleViewModelBase _currentViewModel;
        /// <summary>
        /// disposes the old viewmodel before setting the new one during navigation, in order to free all eventhandlers
        /// </summary>
        public SimpleViewModelBase CurrentViewModel
        {
            get => _currentViewModel;
            set
            {
                _currentViewModel?.Dispose();
                _currentViewModel = value;
                OnCurrentViewModelChanged();
            }
        }

        public event Action CurrentViewModelChanged;

        private void OnCurrentViewModelChanged()
        {
            CurrentViewModelChanged?.Invoke();
        }
    }
}
