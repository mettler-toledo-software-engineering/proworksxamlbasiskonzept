﻿using MT.Singularity.Platform.UI.Navigation;
using System;

namespace mt.proxaml.wpf.Views
{
    /// <summary>
    /// None of the methods are needed and are used in any way.
    /// </summary>
    public partial class HomeScreenView : INavigationPage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HomeScreenPage"/> class.
        /// </summary>
        public HomeScreenView()
        {
            InitializeComponent();
        }

        /// <inheritdoc />
        public event EventHandler CloseRequested;

        /// <inheritdoc />
        public void OnNavigatedTo(NavigationAction action)
        {
        }

        /// <inheritdoc />
        public NavigationResult OnNavigatingFrom(NavigationAction action) => NavigationResult.Proceed;

        /// <inheritdoc />
        public void OnNavigatedFrom(NavigationAction action)
        {
        }

        protected virtual void OnCloseRequested()
        {
            CloseRequested?.Invoke(this, EventArgs.Empty);
        }
    }

}
