﻿using MT.Singularity.Platform.Configuration;

namespace mt.proxaml.wpf.SetupNodes.MainNode
{
    public interface ICustomSetupComponents : IConfigurable<CustomSetupConfiguration>
    {
    }
}
