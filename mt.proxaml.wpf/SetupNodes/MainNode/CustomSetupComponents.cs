﻿using log4net;
using MT.Singularity;
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Platform.UserManagement;

namespace mt.proxaml.wpf.SetupNodes.MainNode
{
    [Export(typeof(IConfigurable))]
    [Export(typeof(ICustomSetupComponents))]
    [InjectionBehavior(IsSingleton = true)]
    public class CustomSetupComponents : ConfigurationStoreConfigurable<CustomSetupConfiguration>, ICustomSetupComponents
    {
        private static readonly ILog _logger = Log4NetManager.ApplicationLogger;
        private const string ThisTypeName = nameof(CustomSetupComponents);

        /// <summary>
        /// Initializes a new instance of the <see cref="MyComponents"/> class.
        /// </summary>
        public CustomSetupComponents(
            IConfigurationStore configurationStore,
            ISecurityService securityService,
            CompositionContainer compositionContainer)
            : base(configurationStore, securityService, compositionContainer)
        {
            _logger.TraceEx(ThisTypeName);
            FriendlyName = nameof(CustomSetupComponents).InsertSeparatorsInCamelCase();

            ConfigurationSelector = nameof(CustomSetupConfiguration);
        }

        /// <inheritdoc cref="ConfigurationStoreConfigurable{TConfiguration}"/>
        public override string FriendlyName { get; }

        /// <inheritdoc cref="ConfigurationStoreConfigurable{TConfiguration}"/>
        public override string ConfigurationSelector { get; }
    }
}
