﻿using mt.proxaml.wpf.SetupNodes.Base;
using mt.proxaml.wpf.SetupNodes.SubNode1;
using mt.proxaml.wpf.SetupNodes.SubNode2;
using MT.Singularity.Composition;
using MT.Singularity.Platform.UI.Shell.Setup;
using MT.Singularity.Platform.UI.Shell.Setup.MenuItems;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace mt.proxaml.wpf.SetupNodes.MainNode
{
    [Export(typeof(ICustomerSetupMenuItem))]
    public class CustomSetupMenuItem : ICustomerSetupMenuItem
    {
        private readonly ISetupMenuContext _setupMenuContext;

        public CustomSetupMenuItem(ISetupMenuContext setupMenuContext)
        {
            _setupMenuContext = setupMenuContext ?? throw new ArgumentNullException(nameof(setupMenuContext));
        }
        public string Title => "My Own Customer Setup Node";

        public string PageTitle => "Hello World Setup";

        public Type ViewModelType => typeof(CustomSetupNodeAViewModel);

        public object ViewModelData { get; }

        public bool HasDefaultDataOnly => false;

        public Task<IEnumerable<ISetupMenuItem>> GetChildrenAsync()
        {
            List<ISetupMenuItem> children = new List<ISetupMenuItem>();

            //General
            List<ISetupMenuItem> submenuGeneral = new List<ISetupMenuItem>();

            //General - Region
            List<ISetupMenuItem> subMenuRegion = new List<ISetupMenuItem>
            {
                new BaseCustomSetupMenuItem("Language", typeof(CustomSetupNodeAViewModel)),
                new BaseCustomSetupMenuItem("Keyboard", typeof(CustomSetupNodeAViewModel))
            };
            submenuGeneral.Add(new BaseCustomSetupMenuItem("Region", subMenuRegion));

            //General - Templates
            List<ISetupMenuItem> subMenuTemplates = new List<ISetupMenuItem>
            {
                new BaseCustomSetupMenuItem("Printer", typeof(PrinterSetupNodeViewModel))
            };
            submenuGeneral.Add(new BaseCustomSetupMenuItem("Templates", subMenuTemplates));

            //General - Test
            submenuGeneral.Add(new BaseCustomSetupMenuItem("Test", typeof(CustomSetupNodeAViewModel)));


            children.Add(new BaseCustomSetupMenuItem("General", submenuGeneral));


            //Database
            children.Add(new BaseCustomSetupMenuItem("Database", typeof(CustomSetupNodeAViewModel)));

            return Task.FromResult((IEnumerable<ISetupMenuItem>)children);
        }

        public Task<bool> IsLeafNode() => Task.FromResult(false);
    }
}
