﻿using System;
using System.ComponentModel;
using mt.proxaml.aprprinter;
using MT.Singularity.Collections;
using MT.Singularity.Components;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Platform.Configuration.Attributes;

namespace mt.proxaml.wpf.SetupNodes.MainNode
{
    public class CustomSetupConfiguration : ComponentConfiguration
    {


        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [DefaultValue("First Setup Entry")]
        public virtual string MyStringValue
        {
            get => (string)GetLocal();
            set => SetLocal(value);
        }

        [Setting(WritePermissions = Permissions.UntilAdministratorId)]
        [DefaultValue("Hello World")]
        public virtual string MySecondString
        {
            get => (string)GetLocal();
            set => SetLocal(value);
        }

        [Setting(WritePermissions = Permissions.UntilAdministratorId)]
        [DefaultValue(10.0)]
        public virtual double MyDouble
        {
            get => (double)GetLocal();
            set => SetLocal(value);
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(4)]
        public virtual int PrinterPort
        {
            get { return (int)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(Resistance.Class2)]
        [Enabled("Enabled")]
        // ReSharper disable once InconsistentNaming
        public virtual Resistance PrinterResistance
        {
            get => (Resistance)GetLocal();
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(Contrast.K4)]
        [Enabled("Enabled")]
        // ReSharper disable once InconsistentNaming
        public virtual Contrast PrinterContrast
        {
            get => (Contrast)GetLocal();
            set { SetLocal(value); }
        }
    }
}
