﻿using System;
using System.ComponentModel;
using MT.Singularity.Composition;
using System.Threading.Tasks;
using System.Windows.Input;
using mt.proxaml.application.Services.SystemUpdate;
using mt.proxaml.wpf.Commands;
using mt.proxaml.wpf.SetupNodes.MainNode;
using MT.Singularity.Platform.UI.Shell.CommonControl;
using MT.Singularity.Platform.UI.Shell.Setup.Pages;
using MT.Singularity.Presentation.UserInteraction;

namespace mt.proxaml.wpf.SetupNodes.SubNode1
{
    [Export(typeof(CustomSetupNodeAViewModel))]
    public class CustomSetupNodeAViewModel : ConfigurationPanelBase<ICustomSetupComponents, CustomSetupConfiguration>
    {
        private readonly ICustomSetupComponents _customSetupComponents;
        public ICommand UpdateSystemCommand { get; }

        public CustomSetupNodeAViewModel(CompositionContainer compositionContainer, IUserInteraction userInteraction, ICustomSetupComponents customSetupComponents) : base(compositionContainer, userInteraction)
        {
            _customSetupComponents = customSetupComponents ?? throw new ArgumentNullException(nameof(customSetupComponents));

            UpdateSystemCommand = new UpdateSystemCommand(this, compositionContainer.Resolve<ISystemUpdateService>());
        }



        public ConfigurationProperty<string> MyStringValue { get; private set; }

        public ConfigurationProperty<string> MySecondString { get; private set; }
        public ConfigurationProperty<double> MyDouble { get; private set; }

        
        private string _updateResult;
        public string UpdateResult
        {
            get => _updateResult;
            set => SetField(ref _updateResult, value);
        }

        public override string Caption => null;

        protected override async Task LoadConfigurationAsync(object context)
        {
            Configurable = _customSetupComponents;

            if (Configurable != null)
            {
                Configuration = await Configurable.GetConfigurationToChangeAsync();
                MyStringValue = new ConfigurationProperty<string>(this, Configuration, nameof(Configuration.MyStringValue));
                MySecondString = new ConfigurationProperty<string>(this, Configuration, nameof(Configuration.MySecondString));
                MyDouble = new ConfigurationProperty<double>(this, Configuration, nameof(Configuration.MyDouble));
            }
        }

    }
}
