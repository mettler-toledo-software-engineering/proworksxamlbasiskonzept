﻿using mt.proxaml.wpf.SetupNodes.MainNode;
using MT.Singularity.Platform.UI.Shell.CommonControl;
using MT.Singularity.Platform.UI.Shell.Setup.Pages;
using MT.Singularity.Presentation.UserInteraction;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using mt.proxaml.aprprinter;
using MT.Singularity.Composition;
using MT.Singularity.Platform.UI.Shell.Setup.Pages.Terminal.Windows;
using MT.Singularity.Translation;

namespace mt.proxaml.wpf.SetupNodes.SubNode2
{
    [Export(typeof(PrinterSetupNodeViewModel))]
    public class PrinterSetupNodeViewModel : ConfigurationPanelBase<ICustomSetupComponents, CustomSetupConfiguration>
    {
        private readonly ICustomSetupComponents _customSetupComponents;

        public PrinterSetupNodeViewModel(CompositionContainer compositionContainer, IUserInteraction userInteraction, ICustomSetupComponents customSetupComponents) : base(compositionContainer, userInteraction)
        {
            _customSetupComponents = customSetupComponents ?? throw new ArgumentNullException(nameof(customSetupComponents));
        }



        public ConfigurationProperty<int> PrinterPort { get; private set; }

        public ConfigurationPropertyEnumSelector<Resistance> PrinterResistance { get; private set; }
        public ConfigurationPropertyEnumSelector<Contrast> PrinterContrast { get; private set; }

        public override string Caption => null;

        protected override async Task LoadConfigurationAsync(object context)
        {
            Configurable = _customSetupComponents;

            if (Configurable != null)
            {
                Configuration = await Configurable.GetConfigurationToChangeAsync();
                
                PrinterPort = new ConfigurationProperty<int>(this, Configuration, nameof(Configuration.PrinterPort));
                PrinterResistance = new ConfigurationPropertyEnumSelector<Resistance>(
                    this,
                    Configuration,
                    nameof(Configuration.PrinterResistance),
                    SingularityTranslation.Key.Print
                    );
                PrinterContrast = new ConfigurationPropertyEnumSelector<Contrast>(
                    this,
                    Configuration,
                    nameof(Configuration.PrinterContrast),
                    SingularityTranslation.Key.Print
                );
            }
        }

    }
}
