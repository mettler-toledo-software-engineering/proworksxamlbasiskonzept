﻿using MT.Singularity.Platform.UI.Infrastructure;

namespace mt.proxaml.wpf.SetupNodes.SubNode2
{
    /// <summary>
    /// Interaction logic for CustomSetupNodeBView.xaml
    /// </summary>
    [SupportedViewModel(typeof(PrinterSetupNodeViewModel))]
    public partial class PrinterSetupNodeView
    {
        internal readonly PrinterSetupNodeViewModel ViewModel;
        public PrinterSetupNodeView(PrinterSetupNodeViewModel viewModel)
        {
            DataContext = ViewModel = viewModel;

            InitializeComponent();
        }
    }
}
