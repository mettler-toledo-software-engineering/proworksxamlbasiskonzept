﻿using MT.Singularity.Platform.UI.Shell.Setup;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace mt.proxaml.wpf.SetupNodes.Base
{
    class BaseCustomSetupMenuItem : ICustomerSetupMenuItem
    {
        private readonly string _title;
        private readonly bool _isLeaf;
        private IEnumerable<ISetupMenuItem> _children = new List<ISetupMenuItem>();

        public BaseCustomSetupMenuItem(string title, IEnumerable<ISetupMenuItem> children)
        {
            _title = title;
            ViewModelType = null;
            _isLeaf = false;
            _children = children;
        }

        public BaseCustomSetupMenuItem(string title, Type viewModelType)
        {
            _title = title;
            ViewModelType = viewModelType;
            _isLeaf = true;
        }

        public string Title => _isLeaf ? _title : "- " + _title + " -";

        public Type ViewModelType { get; }

        public object ViewModelData { get; }

        public bool HasDefaultDataOnly => false;

        public Task<IEnumerable<ISetupMenuItem>> GetChildrenAsync()
        {
            return Task.FromResult(_children);
        }

        public Task<bool> IsLeafNode()
        {
            return Task.FromResult(_isLeaf);
        }
    }
}