﻿using MT.Singularity.Platform.UI.Infrastructure;
using MT.Singularity.Composition;

namespace mt.proxaml.wpf.SetupNodes
{
    [Export(typeof(IViewModelMappingsProvider))]
    public class ViewModelMappingsProvider : AttributedViewModelMappingsProviderBase
    {
    }
}
