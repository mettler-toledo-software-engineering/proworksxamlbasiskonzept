﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using mt.proxaml.persistence.Context;
using mt.proxaml.wpf.CompositionContainerExtensions;
using MT.Singularity.Platform.UI.Shell.WPF.Infrastructure;

namespace mt.proxaml.wpf
{
    public class App : ApplicationBase
    {

        private readonly IHost _host;
        /// <summary>
        /// Application Entry Point
        /// </summary>
        [System.STAThreadAttribute]
        [System.Diagnostics.DebuggerNonUserCodeAttribute]
        public static void Main()
        {

            //homescreenview manages the relations between all views and viewmodels
            //homescreen viewmodel adds all services and dependencies to the composition container, because it is not available during construction of App
            var app = new App();
            app.Run();

        }

        
        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        public App() : base(new ClientBootstrapper(createConfigurationDatabase: true))
        {


            //host builder is used in order to make use of pacakage manager console
            //add-migration name 
            //remove-migration name
            //update-database
            //commands can be used in package manager console with default project set to persistence
            //the following nuget packages need to be installed within the wpf project:
            //Microsoft.Extensions.Hosting, Microsoft.Extensions.DependencyInjection, Microsoft.EntityFrameworkCore.Design


            _host = CreateHostBuilder().Build();
            
            _host.Start();

            TestDbContextFactory contextFactory = _host.Services.GetRequiredService<TestDbContextFactory>();

            //the database is created based on the latest migration in case it is missing
            using (TestDbContext dbcontext = contextFactory.CreateDbContext())
            {
                dbcontext.Database.Migrate();
            }

            //contextfactory is added to composition container, to make it available in all other registered services
            Container.AddInstance(contextFactory);
        }

        public static IHostBuilder CreateHostBuilder(string[] args = null)
        {
            return Host.CreateDefaultBuilder(args)
                .AddDbContext();

        }

    }
}
