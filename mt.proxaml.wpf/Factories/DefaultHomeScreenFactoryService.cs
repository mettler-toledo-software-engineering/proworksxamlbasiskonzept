﻿using MT.Singularity.Composition;
using MT.Singularity.Platform.UI.Navigation;
using MT.Singularity.Platform.UI.Shell.WPF.Infrastructure;
using System.Threading.Tasks;
using mt.proxaml.wpf.Views;

namespace mt.proxaml.wpf.Factories
{
    /// <inheritdoc />
    [Export(typeof(IHomeScreenFactoryService))]
    public class DefaultHomeScreenFactoryService : IHomeScreenFactoryService
    {
        private INavigationPage _homeScreenView;

        /// <inheritdoc />
        public Task<INavigationPage> GetHomeScreenPageAsync()
        {
            _homeScreenView = _homeScreenView ?? new HomeScreenView();

            return Task.FromResult(_homeScreenView);
        }
    }
}
