﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.proxaml.wpf.ViewModels;
using MT.Singularity.Platform.UI.Component.Input;

namespace mt.proxaml.wpf.Commands
{
    public class EditTransactionCommand : AsyncCommandBase
    {
        private readonly SecondViewModel _viewModel;
        private readonly IAlphaNumericInputDialogService _inputDialog;

        public EditTransactionCommand(SecondViewModel viewModel, IAlphaNumericInputDialogService inputDialog)
        {
            _viewModel = viewModel;
            _inputDialog = inputDialog;
        }
        protected override async Task ExecuteAsync(object parameter)
        {
            var result = await _inputDialog.ShowAsync("Enter something");
            _viewModel.Text = result.Text;
        }
    }
}
