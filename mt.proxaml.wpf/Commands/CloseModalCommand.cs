﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.proxaml.wpf.Services;

namespace mt.proxaml.wpf.Commands
{
    public class CloseModalCommand : CommandBase
    {
        private readonly INavigationService _closeModalNavigationService;
        public CloseModalCommand(INavigationService closeModalNavigationService)
        {
            _closeModalNavigationService = closeModalNavigationService;
        }
        public override void Execute(object parameter)
        {
            _closeModalNavigationService.Navigate();
        }
    }
}
