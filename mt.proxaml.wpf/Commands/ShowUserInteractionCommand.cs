﻿using mt.proxaml.wpf.Infrastructure;
using mt.proxaml.wpf.Stores;
using MT.Singularity.Presentation.UserInteraction;
using System.Threading.Tasks;

namespace mt.proxaml.wpf.Commands
{
    /// <summary>
    /// example command to show a default popup window with language dependent text
    /// </summary>
    public class ShowUserInteractionCommand : AsyncCommandBase
    {
        private readonly LanguageStore _languageStore;
        private readonly IUserInteraction _userInteraction;

        public ShowUserInteractionCommand(LanguageStore languageStore, IUserInteraction userInteraction)
        {
            _languageStore = languageStore;
            _userInteraction = userInteraction;
        }
        protected override async Task ExecuteAsync(object parameter)
        {
            var result = await _userInteraction.ShowUserInteractionAsync((string)_languageStore.CurrentLanguageDictionary[Globals.MESSAGE_TEXT], (string)_languageStore.CurrentLanguageDictionary[Globals.MESSAGE_TITLE], UserInteractionChoices.OKCancel, UserInteractionIcon.Information);

        }
    }
}
