﻿using System;
using System.Threading.Tasks;
using MT.Singularity.Platform.Ribbon;

namespace mt.proxaml.wpf.Commands
{

    /// <summary>
    /// multi purpose base command, can be used as Regular ICommand or IRibbonCommand
    /// Sets Can Execute to false as long as a Task is running async
    /// </summary>
    public abstract class AsyncCommandBase : CommandBase, IRibbonCommand
    {
        private readonly Action<Exception> _onException;

        private bool _isExecuting;
        public bool IsExecuting
        {
            get
            {
                return _isExecuting;
            }
            private set
            {
                _isExecuting = value;
                OnCanExecuteChanged();
            }
        }

        bool IRibbonCommand.CanExecute => CanExecute(null);

        public AsyncCommandBase(Action<Exception> onException = null)
        {
            _onException = onException;
        }

        public override bool CanExecute(object parameter)
        {
            return !IsExecuting && base.CanExecute(parameter);
        }

        public override async void Execute(object parameter)
        {
            await ExecutingAsync(parameter);
        }

        protected abstract Task ExecuteAsync(object parameter);

        async Task IRibbonCommand.ExecuteCommandAsync()
        {
            await ExecutingAsync();
        }

        private async Task ExecutingAsync(object parameter = null)
        {
            IsExecuting = true;

            try
            {
                await ExecuteAsync(null);
            }
            catch (Exception ex)
            {
                _onException?.Invoke(ex);
            }

            IsExecuting = false;
        }
    }
}
