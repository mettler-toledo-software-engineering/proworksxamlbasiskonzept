﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.proxaml.application.Services.SystemUpdate;
using mt.proxaml.wpf.SetupNodes.SubNode1;

namespace mt.proxaml.wpf.Commands
{
    public class UpdateSystemCommand : CommandBase
    {
        private readonly CustomSetupNodeAViewModel _viewModel;
        private readonly ISystemUpdateService _systemUpdateService;

        public UpdateSystemCommand(CustomSetupNodeAViewModel viewModel,ISystemUpdateService systemUpdateService)
        {
            _viewModel = viewModel;
            _systemUpdateService = systemUpdateService;
        }

        public override void Execute(object parameter)
        {
            try
            {
                var success = _systemUpdateService.ExecuteUpdate();
                if (success)
                {
                    _viewModel.UpdateResult = "Update Success";
                    return;
                }

                _viewModel.UpdateResult = "Update Failed";
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
