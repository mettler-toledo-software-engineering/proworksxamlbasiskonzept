﻿using MT.Singularity.Platform.Ribbon;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.proxaml.wpf.Services;
using mt.proxaml.wpf.Stores;
using mt.proxaml.wpf.ViewModels;

namespace mt.proxaml.wpf.Commands
{
    /// <summary>
    /// naviagation example with a condition to only navigate if a certain property of a viewmodel is filled
    /// </summary>
    public class NavigateFirstViewModelCommand : CommandBase, IRibbonCommand
    {
        private readonly GlobalMessageStore _messagestore;
        private readonly SecondViewModel _viewModel;
        private readonly INavigationService _navigationService;
        private bool _canExecute = false;

        public NavigateFirstViewModelCommand(GlobalMessageStore messagestore, SecondViewModel viewModel, INavigationService navigationService)
        {
            _messagestore = messagestore;
            _viewModel = viewModel;
            _navigationService = navigationService;
            _viewModel.PropertyChanged += ViewModelOnPropertyChanged;
        }

        private void ViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnCanExecuteChanged();
        }

        bool IRibbonCommand.CanExecute => CanExecute(null);

        public override bool CanExecute(object parameter)
        {
            _canExecute = string.IsNullOrWhiteSpace(_viewModel.Text) == false;

            if (_canExecute == false)
            {
                _messagestore.UpdateMessage("text field must not be empty",GlobalMessageStore.MessageType.Error, 1);
            }

            return base.CanExecute(parameter) && _canExecute;
        }


        public override void Execute(object parameter)
        {
            _navigationService.Navigate();
        }

        public Task ExecuteCommandAsync()
        {
            Execute(null);
            return Task.CompletedTask;

        }
    }
}
