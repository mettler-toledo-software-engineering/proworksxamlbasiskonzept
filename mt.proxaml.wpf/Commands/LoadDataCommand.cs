﻿using mt.proxaml.wpf.Stores;
using MT.Singularity.Platform.Devices.Scale;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mt.proxaml.wpf.Commands
{
    /// <summary>
    /// is executed by the code behind of a view in conjunction with a dependency property where data needs to be loaded from a database
    /// </summary>
    public class LoadDataCommand : AsyncCommandBase
    {
        private readonly IDataStore _dataStore;
        private readonly GlobalMessageStore _messageStore;

        public LoadDataCommand(IDataStore dataStoreStore, GlobalMessageStore messageStore)
        {
            _dataStore = dataStoreStore;
            _messageStore = messageStore;
        }

        protected override async Task ExecuteAsync(object parameter)
        {
            try
            {
                await _dataStore.LoadData();
            }
            catch (Exception e)
            {
                _messageStore.UpdateMessage("Loading of Data Failed", GlobalMessageStore.MessageType.Error, e);
            }
        }
    }
}