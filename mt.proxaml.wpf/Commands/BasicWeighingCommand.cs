﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.proxaml.wpf.Infrastructure;
using mt.proxaml.wpf.ViewModels;
using MT.Singularity.Presentation.UserInteraction;

namespace mt.proxaml.wpf.Commands
{
    /// <summary>
    /// example command to show a default popup window with language dependent text
    /// </summary>
    public class BasicWeighingCommand //: AsyncCommandBase
    {
        private readonly BasicWeighingViewModel _viewModel;
        private readonly IUserInteraction _userInteraction;

        public BasicWeighingCommand(BasicWeighingViewModel viewModel, IUserInteraction userInteraction)
        {
            _viewModel = viewModel;
            _userInteraction = userInteraction;
        }
        protected  /*override async Task*/ void ExecuteAsync(object parameter)
        {
            throw new NotImplementedException();
          //  var result = await _userInteraction.ShowUserInteractionAsync((string)_viewModel.CurrentDictionary[Globals.MESSAGE_TEXT], (string)_viewModel.CurrentDictionary[Globals.MESSAGE_TITLE], UserInteractionChoices.OKCancel, UserInteractionIcon.Information);

        }
    }
}
