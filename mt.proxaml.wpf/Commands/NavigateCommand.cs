﻿using System.Threading.Tasks;
using mt.proxaml.wpf.Services;
using MT.Singularity.Platform.Ribbon;

namespace mt.proxaml.wpf.Commands
{
    /// <summary>
    /// default navigate command, can be used if no advanced logic is needed
    /// </summary>
    public class NavigateCommand : CommandBase, IRibbonCommand
    {
        private readonly INavigationService _navigationService;
        private bool _canExecute = true;

        public NavigateCommand(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        bool IRibbonCommand.CanExecute => CanExecute(null);

        public override bool CanExecute(object parameter)
        {
            return base.CanExecute(parameter) && _canExecute;
        }


        public override void Execute(object parameter)
        {
            _navigationService.Navigate();
        }

        public Task ExecuteCommandAsync()
        {
            Execute(null);
            return Task.CompletedTask;
            
        }
    }
}
