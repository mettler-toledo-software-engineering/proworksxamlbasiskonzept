﻿using mt.proxaml.wpf.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.proxaml.application.Models;
using mt.proxaml.wpf.ViewModels;

namespace mt.proxaml.wpf.Commands
{
    public class DeleteTransactionCommand : AsyncCommandBase
    {
        private readonly SecondViewModel _viewModel;
        private readonly TransactionStore _transactionStore;
        private readonly GlobalMessageStore _messageStore;

        public DeleteTransactionCommand(SecondViewModel viewModel ,TransactionStore transactionStore, GlobalMessageStore messageStore)
        {
            _viewModel = viewModel;
            _transactionStore = transactionStore;
            _messageStore = messageStore;
        }
        protected override async Task ExecuteAsync(object parameter)
        {
            try
            {
                var transaction = (_viewModel.SelectedItem as TransactionViewModel)?.SourceTransaction;
                await _transactionStore.DeleteTransaction(transaction);
                _messageStore.UpdateMessage("Transaction deleted", GlobalMessageStore.MessageType.Success);
            }
            catch (Exception e)
            {
                _messageStore.UpdateMessage("Delete failed", GlobalMessageStore.MessageType.Error, e);

            }


        }
    }
}
