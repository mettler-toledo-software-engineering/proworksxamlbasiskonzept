﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using mt.proxaml.application.Models;
using mt.proxaml.wpf.Stores;

namespace mt.proxaml.wpf.Commands
{
    public class AddTransactionCommand : AsyncCommandBase
    {
        private readonly TransactionStore _transactionStore;
        private readonly GlobalMessageStore _messageStore;

        public AddTransactionCommand(TransactionStore transactionStore, GlobalMessageStore messageStore)
        {
            _transactionStore = transactionStore;
            _messageStore = messageStore;
        }
        protected override async Task ExecuteAsync(object parameter)
        {
            try
            {
                var transaction = new Transaction();
                transaction.TimeStamp = DateTime.Now;
                await _transactionStore.CreateTransaction(transaction);
                _messageStore.UpdateMessage("Transaction added", GlobalMessageStore.MessageType.Success);
            }
            catch (Exception e)
            {
                _messageStore.UpdateMessage("Transaction failed", GlobalMessageStore.MessageType.Error,e);

            }
            

        }
    }
}
