﻿namespace mt.proxaml.wpf.Services
{
    public interface INavigationService
    {
        void Navigate();
    }
}
