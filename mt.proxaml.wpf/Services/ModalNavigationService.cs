﻿using mt.proxaml.wpf.Stores;
using System;
using mt.proxaml.wpf.ViewModels;

namespace mt.proxaml.wpf.Services
{
    public class ModalNavigationService<TViewModel> : INavigationService
        where TViewModel : SimpleViewModelBase
    {
        private readonly ModalNavigationStore _navigationStore;
        private readonly Func<TViewModel> _createViewModel;

        public ModalNavigationService(ModalNavigationStore navigationStore, Func<TViewModel> createViewModel)
        {
            _navigationStore = navigationStore;
            _createViewModel = createViewModel;
        }

        public void Navigate()
        {
            _navigationStore.CurrentViewModel = _createViewModel();
        }
    }
}
