﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using mt.proxaml.persistence.Context;
using mt.proxaml.wpf.Services;
using MT.Singularity.Composition;

namespace mt.proxaml.wpf.CompositionContainerExtensions
{
    public static class AddDbContextToCompositionContainerExtension
    {


        public static IHostBuilder AddDbContext(this IHostBuilder host)
        {
            host.ConfigureServices((context, services) =>
            {
                string connectionString = ConfigurationManager.AppSettings["connectionstring"];
                Action<DbContextOptionsBuilder> configureDbContext = o => o.UseSqlite(connectionString);

                services.AddDbContext<TestDbContext>(configureDbContext);
                services.AddSingleton<TestDbContextFactory>(new TestDbContextFactory(configureDbContext));


            });

            return host;
        }

    }
}
