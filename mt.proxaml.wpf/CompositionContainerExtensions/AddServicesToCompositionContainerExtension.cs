﻿using mt.proxaml.application.Services.DataAccess;
using mt.proxaml.persistence.Context;
using mt.proxaml.persistence.Services;
using mt.proxaml.wpf.Services;
using mt.proxaml.wpf.Stores;
using mt.proxaml.wpf.ViewModels;
using MT.Singularity.Composition;
using System;
using Microsoft.Extensions.DependencyInjection;
using mt.proxaml.application.Services.SystemUpdate;
using mt.proxaml.aprprinter;
using MT.Singularity.Platform.Devices;

namespace mt.proxaml.wpf.CompositionContainerExtensions
{
    public static class AddServicesToCompositionContainerExtension
    {
        public static CompositionContainer AddServicesToCompositionContainer(this CompositionContainer container)
        {
            container.AddInstance<INavigationService>(CreateBasicWeighingNavigationService(container));
           // container.AddInstance<INavigationService>(CreateFirstNavigationService(container));
            container.AddInstance<ISystemUpdateService>(new SystemUpdateService());
            
            container.AddInstance<ITransactionService>(new TransactionDataService(container.Resolve<TestDbContextFactory>()));
            container.AddInstance(new CloseModalNavigationService(container.Resolve<ModalNavigationStore>()));


            container.AddInstance<IPrinterFactory>(new PrinterFactory(container.Resolve<IInterfaceService>()));

            return container;
        }


        public static INavigationService CreateBasicWeighingNavigationService(IObjectResolver container)
        {
            return new NavigationService<BasicWeighingViewModel>(container.Resolve<NavigationStore>(), container.Resolve<BasicWeighingViewModel>);
        }

        public static INavigationService CreateFirstNavigationService(IObjectResolver container)
        {
            return new NavigationService<FirstViewModel>(container.Resolve<NavigationStore>(), container.Resolve<FirstViewModel>);
        }

        public static INavigationService CreateSecondNavigationService(IObjectResolver container)
        {
            return new NavigationService<SecondViewModel>(container.Resolve<NavigationStore>(), container.Resolve<SecondViewModel>);
        }

        public static INavigationService CreateModalNavigationService(IObjectResolver container)
        {
            return new ModalNavigationService<ModalViewModel>(
                container.Resolve<ModalNavigationStore>(), container.Resolve<ModalViewModel>);
        }
    }
}
