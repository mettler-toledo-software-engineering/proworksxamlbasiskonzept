﻿using mt.proxaml.application.Services.DataAccess;
using mt.proxaml.aprprinter;
using mt.proxaml.wpf.Stores;
using mt.proxaml.wpf.SetupNodes.MainNode;
using MT.Singularity.Composition;
using MT.Singularity.Platform.Application.Value;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Platform.UserManagement;
using MT.Singularity.Platform.UI.Shell.Ribbon;

namespace mt.proxaml.wpf.CompositionContainerExtensions
{
    public static class AddStoresToCompositonContainerExtension
    {
        public static CompositionContainer AddStoresToCompositonContainer(this CompositionContainer container)
        {
            container.AddInstance(new LanguageStore());
            
            
            
            container.AddInstance(new TransactionStore(container.Resolve<ITransactionService>()));
            container.AddInstance(new UserStore(container.Resolve<ISecurityService>()));
            container.AddInstance(new CustomSetupStore(container.Resolve<ICustomSetupComponents>()));

            container.AddInstance(new PrinterStore(container.Resolve<CustomSetupStore>(), container.Resolve<IPrinterFactory>()));

            container.AddInstance(new RibbonItemStore(container.Resolve<RibbonItemsProvider>()));
            container.AddInstance(new ScaleStore(container.Resolve<IScaleService>(), container.Resolve<IApplicationValueService>(), container.Resolve<GlobalMessageStore>(), container.Resolve<LanguageStore>()));

            //the following stores are used by the homescreen view model and
            //are registered by export functionality only to avoid different instance registrations
            //container.AddInstance(new NavigationStore());
            //container.AddInstance(new GlobalMessageStore(container.Resolve<IEngineMessageService>()));
            //container.AddInstance(new ModalNavigationStore());

            return container;
        }
    }
}
