﻿using mt.proxaml.wpf.Services;
using mt.proxaml.wpf.Stores;
using mt.proxaml.wpf.ViewModels;
using MT.Singularity.Composition;
using MT.Singularity.Platform.UI.Component.Input;
using MT.Singularity.Platform.UserManagement;
using MT.Singularity.Presentation.UserInteraction;

namespace mt.proxaml.wpf.CompositionContainerExtensions
{
    public static class AddViewModelsToCompositonContainerExtension
    {
        public static CompositionContainer AddViewModelsToCompositonContainer(this CompositionContainer container)
        {
            container.AddFactory(() => CreateBasicWeighingViewModel(container));
            container.AddFactory(() => CreateFirstViewModel(container));
            container.AddFactory(() => CreateSecondViewModel(container));
            container.AddFactory(() => CreateModalViewModel(container));
            container.AddInstance(CreateGlobalMessageViewModel(container));

            return container;
        }

        public static ModalViewModel CreateModalViewModel(IObjectResolver resolver)
        {
            return new ModalViewModel(resolver.Resolve<CloseModalNavigationService>());
        }

        public static BasicWeighingViewModel CreateBasicWeighingViewModel(IObjectResolver resolver)
        {
            return new BasicWeighingViewModel(resolver.Resolve<ScaleStore>(), resolver.Resolve<LanguageStore>(), resolver.Resolve<RibbonItemStore>(), AddServicesToCompositionContainerExtension.CreateSecondNavigationService(resolver), resolver.Resolve<IUserInteraction>(), resolver.Resolve<ISecurityService>());
        }

        public static FirstViewModel CreateFirstViewModel(IObjectResolver resolver)
        {
            return new FirstViewModel(resolver.Resolve<LanguageStore>(), resolver.Resolve<RibbonItemStore>(), AddServicesToCompositionContainerExtension.CreateSecondNavigationService(resolver), AddServicesToCompositionContainerExtension.CreateBasicWeighingNavigationService(resolver), resolver.Resolve<IUserInteraction>());
        }

        public static SecondViewModel CreateSecondViewModel(IObjectResolver resolver)
        {
            return new SecondViewModel(
                resolver.Resolve<RibbonItemStore>(), 
                AddServicesToCompositionContainerExtension.CreateFirstNavigationService(resolver), 
                resolver.Resolve<TransactionStore>(),
                resolver.Resolve<GlobalMessageStore>(),
                AddServicesToCompositionContainerExtension.CreateModalNavigationService(resolver),
                resolver.Resolve<IAlphaNumericInputDialogService>()
            );
        }

        private static GlobalMessageViewModel CreateGlobalMessageViewModel(IObjectResolver resolver)
        {
            return new GlobalMessageViewModel(resolver.Resolve<GlobalMessageStore>());
        }
    }
}
