﻿using System;
using System.Windows;
using mt.proxaml.wpf.Stores;
using MT.Singularity.Composition;
using MT.Singularity.Platform.UI;

namespace mt.proxaml.wpf.ViewModels
{
    /// <summary>
    /// extends the singluratiy viewmodelbase with idisposable and language handling
    /// currently not in use, simpleviewmodelbase can be used for all viewmodels except the homescreenviewmodel
    /// </summary>
    public abstract class ExtendedViewModelBase : ViewModelBase, IDisposable
    {
        protected ResourceDictionary CurrentDictionary;
        private readonly LanguageStore _languageStore;
        protected ExtendedViewModelBase(IObjectResolver compositionContainer) : base(compositionContainer)
        {
            _languageStore = compositionContainer.Resolve<LanguageStore>();
            _languageStore.LanguageChanged += LanguageStoreOnLanguageChanged;
            CurrentDictionary = _languageStore.CurrentLanguageDictionary;
        }

        private void LanguageStoreOnLanguageChanged()
        {
            CurrentDictionary = _languageStore.CurrentLanguageDictionary;
        }

        public virtual void Dispose()
        {
            _languageStore.LanguageChanged -= LanguageStoreOnLanguageChanged;
        }


    }
}
