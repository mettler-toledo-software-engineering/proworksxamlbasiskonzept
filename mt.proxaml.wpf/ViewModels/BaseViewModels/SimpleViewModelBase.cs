﻿using MT.Singularity.Data;
using System;

namespace mt.proxaml.wpf.ViewModels
{
    /// <summary>
    /// this base can be used, when no singularity features are necessary
    /// </summary>
    public abstract class SimpleViewModelBase : PropertyChangedBase, IDisposable
    {

        public SimpleViewModelBase()
        {
            
        }

        public virtual void Dispose()
        {


        }
    }    
}
