﻿using mt.proxaml.wpf.CustomItems;
using MT.Singularity.Platform.UI.Component.Ribbon;
using MT.Singularity.Platform.UI.Shell.Ribbon;
using System.Collections.ObjectModel;

namespace mt.proxaml.wpf.ViewModels
{
    public abstract class RibbonViewModelBase : SimpleViewModelBase
    {
        public RibbonViewModelBase()
        {
            ClearRibbonItems();
        }


        public void ClearRibbonItems()
        {
            _ribbonItems.Clear();
        }
        public void AddRibbonItem(RibbonItem item)
        {
            _ribbonItems.Add(item);
        }

        public void SetRibbonItem(int index, RibbonItem item)
        {
            if (_ribbonItems.Count == index)
                AddRibbonItem(item);
            else
            {
                if (_ribbonItems.Count < index)
                {
                    int start = _ribbonItems.Count;
                    for (int i = start; i < index; i++)
                    {
                        AddRibbonItem(new RibbonItem("PlaceHolder"));
                    }
                    AddRibbonItem(item);
                }
                else
                    _ribbonItems[index] = item;
            }
        }

        public RibbonItem GetRibbonItem(int index)
        {
            if (index <= _ribbonItems.Count)
            {
                return _ribbonItems[index];
            }
            return null;
        }

        internal RibbonItem GetRibbonItem(string id)
        {
            foreach (var item in _ribbonItems)
            {
                if (item.Id == id)
                    return item;
            }
            return null;
        }

        internal RibbonItem CloneRibbonItem(string id)
        {
            RibbonItem orig = GetRibbonItem(id);
            RibbonItem result = CreateRibbonItem(id);

            result.Command = orig.Command;
            result.LongPressCommand = orig.LongPressCommand;
            result.Text = orig.Text.Clone() as string;
            result.ImageSource = orig.ImageSource.Clone() as string;
            result.IsChecked = orig.IsChecked;
            result.IsEnabled = orig.IsEnabled;
            result.IsFlyOutEnabled = orig.IsFlyOutEnabled;
            result.FlyOutItems = orig.FlyOutItems;
            return result;
        }

        public RibbonItem CreateRibbonItem(string id)
        {
            return new RibbonItem(id);
        }

        public ObservableCollection<IRibbonItem> RibbonItems
        {
            get
            {
                ExtendedObservableCollection<IRibbonItem> result = new ExtendedObservableCollection<IRibbonItem>();
                foreach (var item in _ribbonItems)
                {
                    result.Add(item);
                }
                return result;
            }
        }
        private readonly ExtendedObservableCollection<RibbonItem> _ribbonItems = new ExtendedObservableCollection<RibbonItem>();

        public void RibbonItemsNotifyPropertyChanged()
        {
            NotifyPropertyChanged(nameof(RibbonItems));
        }

        public override void Dispose()
        {
            base.Dispose();
        }
    }
}
