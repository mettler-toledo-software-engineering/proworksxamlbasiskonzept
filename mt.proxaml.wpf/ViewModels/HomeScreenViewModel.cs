﻿using MT.Singularity.Composition;
using MT.Singularity.Expressions;
using MT.Singularity.Platform.UI;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using mt.proxaml.persistence.Context;
using mt.proxaml.wpf.CompositionContainerExtensions;
using mt.proxaml.wpf.Services;
using mt.proxaml.wpf.Stores;

namespace mt.proxaml.wpf.ViewModels
{
    public class HomeScreenViewModel : ViewModelBase
    {
        private readonly BindingsContainer _bindings = new BindingsContainer();

        /// <summary>
        /// single source for all matters regarding navigation
        /// </summary>
        private readonly NavigationStore _navigationStore;
        /// <summary>
        /// single source for all matters regarding modal navigation
        /// </summary>
        private readonly ModalNavigationStore _modalNavigationStore;
        /// <summary>
        /// contains all dependencies, including MT specific Interfaces 
        /// </summary>
        private readonly CompositionContainer _compositionContainer;

        /// <summary>
        /// holds the viewmodel that needs to be displayed
        /// </summary>
        public SimpleViewModelBase CurrentViewModel => _navigationStore.CurrentViewModel;
        /// <summary>
        /// holds the viewmodel that needs to be displayed as a modal
        /// </summary>
        public SimpleViewModelBase CurrentModalViewModel => _modalNavigationStore.CurrentViewModel;
        /// <summary>
        /// modal binds to this property to signal wether the modal is open or not
        /// </summary>
        public bool ModalIsOpen => _modalNavigationStore.ModalIsOpen;
        
        /// <summary>
        /// the viewmodel for the global message banner area, to be displayed at the top of the application
        /// </summary>
        public GlobalMessageViewModel GlobalMessageViewModel => _compositionContainer.Resolve<GlobalMessageViewModel>();
        /// <summary>
        /// Initializes a new instance of the <see cref="HomeScreenViewModel"/> class.
        /// </summary>
        /// <param name="compositionContainer">Composition Container</param>
        /// <param name="navigationStore"></param>
        public HomeScreenViewModel(IObjectResolver compositionContainer) : base(compositionContainer)
        {

            _compositionContainer = compositionContainer.Resolve<CompositionContainer>();
            _navigationStore = compositionContainer.Resolve<NavigationStore>();
            _modalNavigationStore = compositionContainer.Resolve<ModalNavigationStore>();
            _navigationStore.CurrentViewModelChanged += OnCurrentViewModelChanged;
            _modalNavigationStore.CurrentViewModelChanged += OnCurrentModalViewModelChanged;
        }

        /// <summary>
        /// is triggered when a new viewmodel is set as the CurrentViewModel in the Navigation Store after a navigation service navigates to another viewmodel
        /// </summary>
        private void OnCurrentViewModelChanged()
        {
            NotifyPropertyChanged(nameof(CurrentViewModel));
        }

        /// <summary>
        /// is triggered when a new viewmodel is set as the CurrentViewModel in the Modal Navigation Store after a navigation service navigates to another viewmodel
        /// </summary>
        private void OnCurrentModalViewModelChanged()
        {
            NotifyPropertyChanged(nameof(CurrentModalViewModel));
            NotifyPropertyChanged(nameof(ModalIsOpen));
        }

        //the composition container is not fully available during constriction of app.
        //thats why all regsitrations are happening in this viewmodel

        public override Task InitializeAsync(object context)
        {
            _compositionContainer.EnableRecursiveResolution();
            _compositionContainer
                .AddViewModelsToCompositonContainer()
                .AddServicesToCompositionContainer()
                .AddStoresToCompositonContainer()
                .AddViewsToCompositonContainer();




            //navigating to the first view with actual content
            var firstnavigation = _compositionContainer.Resolve<INavigationService>();

            firstnavigation.Navigate();

            return Task.CompletedTask;
        }



        /// <inheritdoc />
        protected override async Task OnShutdownAsync()
        {
            _bindings.Stop();
            await base.OnShutdownAsync();
        }

    }
}
