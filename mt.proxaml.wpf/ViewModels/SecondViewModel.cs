﻿using mt.proxaml.application.Models;
using mt.proxaml.wpf.Commands;
using mt.proxaml.wpf.Infrastructure;
using mt.proxaml.wpf.Services;
using mt.proxaml.wpf.Stores;
using MT.Singularity.Platform.Ribbon;
using MT.Singularity.Platform.UI.Component.Input;
using MT.Singularity.Platform.UI.Shell.Ribbon;
using MT.Singularity.Platform.UI.Toolkit;
using MT.Singularity.Translation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace mt.proxaml.wpf.ViewModels
{

    /// <summary>
    /// example viewmodel to show how to display database data in a grid,
    /// navigation with dependencies, loading data in an efficient way with dependency properties,
    /// context menu in database grid
    /// </summary>
    public class SecondViewModel : RibbonViewModelBase, IDataGridContextCommandProvider
    {


        private readonly RibbonItemStore _ribbonItemStore;
        private readonly TransactionStore _transactionStore;

        private ObservableCollection<TransactionViewModel> _transactions;

        public ObservableCollection<TransactionViewModel> Transactions
        {
            get { return _transactions; }
            set
            {
                _transactions = value;
                NotifyPropertyChanged(nameof(Transactions));
            }
        }




        public ICommand AddCommand { get; }
        public ICommand DeleteCommand { get; }
        public ICommand EditCommand { get; }

        public IRibbonCommand NavigateFirstViewModelCommand { get; }
        public IRibbonCommand AddTransactionCommand { get; }
        public IRibbonCommand OpenModalCommand { get; }

        /// <summary>
        /// is used by dependency property LoadDataCommand in code behind of the corresponding view
        /// dependecy property and Load data command are combined in homescreen view
        /// is executed when view is loaded and onloaded event is called.
        /// </summary>
        public ICommand LoadDataCommand { get; }
        public SecondViewModel(
            RibbonItemStore ribbonItemStore, 
            INavigationService navigateToFirstView, 
            TransactionStore transactionStore, 
            GlobalMessageStore messageStore, 
            INavigationService openModal,
            IAlphaNumericInputDialogService alphaNumericInputDialogService
            ) : base()
        {
            _ribbonItemStore = ribbonItemStore;
            _transactionStore = transactionStore;

            _transactionStore.TransactionsLoaded += UpdateTransactions;
            _transactionStore.TransactionCreated += TransactionCreated;

            NavigateFirstViewModelCommand = new NavigateFirstViewModelCommand(messageStore,this,navigateToFirstView);
            AddTransactionCommand = new AddTransactionCommand(transactionStore, messageStore);
            OpenModalCommand = new NavigateCommand(openModal);
            LoadDataCommand = new LoadDataCommand(transactionStore, messageStore);
            AddCommand = new AddTransactionCommand(transactionStore, messageStore);
            DeleteCommand = new DeleteTransactionCommand(this, transactionStore, messageStore);
            EditCommand = new EditTransactionCommand(this, alphaNumericInputDialogService);
            Transactions = new ObservableCollection<TransactionViewModel>();

            AddRibbonItems();
            
        }

        private void TransactionCreated(Transaction transaction)
        {
            TransactionViewModel transactionViewModel = new TransactionViewModel(transaction);
            Transactions.Insert(0, transactionViewModel);
        }

        private void UpdateTransactions()
        {
            Transactions.Clear();
            _transactionStore.Transactions.ToList().ForEach(t => Transactions.Add(new TransactionViewModel(t)));
        }

        private void AddRibbonItems()
        {
            var openModalkey = new RibbonItem("openModalkey")
            {
                Text = "open Modal",
                ImageSource = "Eye_MTBlue",
                Command = OpenModalCommand
            };

            var addtransactionkey = new RibbonItem("addtransactionkey")
            {
                Text = "Add Transaction",
                ImageSource = "Add_Plus_MTBlue",
                Command = AddTransactionCommand
            };

            var nextViewKey = new RibbonItem("navigatefirstview")
            {
                Text = "To View 1",
                ImageSource = "Arrow_left_MTBlue",
                Command = NavigateFirstViewModelCommand
            };

            SetRibbonItem(0, _ribbonItemStore.ZeroKey);
            SetRibbonItem(1, _ribbonItemStore.Tarekey);
            SetRibbonItem(2, _ribbonItemStore.Clearkey);
            SetRibbonItem(3, _ribbonItemStore.PlaceHolderKey);
            SetRibbonItem(4, _ribbonItemStore.PlaceHolderKey);
            SetRibbonItem(5, openModalkey);
            SetRibbonItem(6, addtransactionkey);
            SetRibbonItem(7, nextViewKey);
        }

        //composes the context menu when clicking on a datagrid item
        //is used and called by implementing the IDataGridContextCommandProvider interface for a class
        public List<Tuple<string, ICommand>> DataGridContextCommands =>
            new List<Tuple<string, ICommand>>
            {
                new Tuple<string, ICommand>(Globals.DeleteIcon, DeleteCommand),
                new Tuple<string, ICommand>(Globals.AddIcon, AddCommand),
                new Tuple<string, ICommand>(Globals.EditIcon, EditCommand),
                //new Tuple<string, ICommand>(Globals.CustomerIcon, CustomerCommand),
            };
        /// <inheritdoc />
        public Tuple<string, string, ICommand> DataGridAddCommand => new Tuple<string, string, ICommand>(
            SingularityTranslation.Get(SingularityTranslation.Key.Add), Globals.AddIcon, AddCommand);



        private string _text;
        public string Text
        {
            
            get { return _text; }
            set => SetField(ref _text, value);
        }

        private string _caption;
        public string Caption
        {

            get { return _caption; }
            set => SetField(ref _caption, value);
        }

        private object _selectedItem;

        public object SelectedItem
        {
            get => _selectedItem;
            set => SetField(ref _selectedItem, value);
        }
        private int _selectedIndex;

        public int SelectedIndex
        {
            get => _selectedIndex;
            set => SetField(ref _selectedIndex, value);
        }

       

        /// <summary>
        /// Gets a value indicating whether controls visibility of add new record row.
        /// </summary>

        private bool _isAddNewRecordVisible = false;
        public bool IsAddNewRecordVisible
        {
            get => _isAddNewRecordVisible;
            // ReSharper disable once UnusedMember.Local Used by data binding.
            private set => SetField(ref _isAddNewRecordVisible, value);
        }

        /// <summary>
        /// is being called when navigating away from this viewmodel
        /// used to clear all event registrations to avoid memory leaks
        /// </summary>

        public override void Dispose()
        {
            _transactionStore.TransactionCreated -= TransactionCreated;
            _transactionStore.TransactionsLoaded -= UpdateTransactions;
            base.Dispose();
        }
    }
}
