﻿using mt.proxaml.wpf.Commands;
using mt.proxaml.wpf.Services;
using mt.proxaml.wpf.Stores;
using MT.Singularity.Platform.Ribbon;
using MT.Singularity.Platform.UI.Component.Ribbon;
using MT.Singularity.Platform.UI.Shell.Ribbon;
using MT.Singularity.Platform.UserManagement;
using MT.Singularity.Presentation.UserInteraction;
using System.Collections.Generic;

namespace mt.proxaml.wpf.ViewModels
{
    public class BasicWeighingViewModel : RibbonViewModelBase
    {

        public IRibbonCommand NavigateSecondViewModelCommand { get; }
        public IRibbonCommand BasicWeighingCommand { get; }

        enum enuSoftkeys
        {
            back = 0,
            Tare = 4,
            Ok = 7,
        }

        private readonly RibbonItemStore _ribbonItemStore;

        private readonly ScaleStore _ScaleStore;

        /// <summary>
        /// example to show base popup windows and langauge handling
        /// </summary>
        /// <param name="languageStore"></param>
        /// <param name="ribbonItemStore"></param>
        /// <param name="navigateToSecondViewModel"></param>
        /// <param name="userInteraction"></param>
        public BasicWeighingViewModel(ScaleStore scaleStore, LanguageStore languageStore, RibbonItemStore ribbonItemStore, INavigationService navigateToSecondViewModel, IUserInteraction userInteraction, ISecurityService securityService) : base()
        {
            _ScaleStore = scaleStore;
            NavigateSecondViewModelCommand = new NavigateCommand(navigateToSecondViewModel);
            _ribbonItemStore = ribbonItemStore;
            securityService.CurrentUserChanged += SecurityService_CurrentUserChanged;
            AddRibbonItems();
        }

        private void SecurityService_CurrentUserChanged(User user)
        {
            if (user.Name == "Admin")
                SetRibbonItem((int)enuSoftkeys.Ok, _ribbonItemStore.Highreskey);
            else
                SetRibbonItem((int)enuSoftkeys.Ok, _ribbonItemStore.PresetTarekey);
            RibbonItemsNotifyPropertyChanged();
        }


        /// <summary>
        /// Fill the ribbon bar with buttons and place holder items where no button is needed
        /// </summary>
        private void AddRibbonItems()
        {
            SetRibbonItem((int)enuSoftkeys.back, new RibbonItem("navigatesecondview")
            {
                Text = "HOME",
                ImageSource = "Arrow_right_MTBlue",
                //   Command = NavigateSecondViewModelCommand,
                IsEnabled = true
            });

            SetRibbonItem((int)enuSoftkeys.Ok, _ribbonItemStore.Printkey);
            SetRibbonItem((int)enuSoftkeys.Tare, _ribbonItemStore.Tarekey);

            SetRibbonItem(2, new RibbonItem("navigatesecondview")
            {
                Text = "FlyOut",
                ImageSource = "Arrow_right_MTBlue",
                IsEnabled = true,
                IsFlyOutEnabled = true,
                FlyOutItems = new List<IRibbonFlyOutItem>()
                {
                    new RibbonFlyOutItem("To View 2", NavigateSecondViewModelCommand)
                }
            });
        }





        public override void Dispose()
        {
            base.Dispose();
        }
    }
}
