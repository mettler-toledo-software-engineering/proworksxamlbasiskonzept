﻿using mt.proxaml.wpf.Commands;
using mt.proxaml.wpf.Infrastructure;
using mt.proxaml.wpf.Services;
using mt.proxaml.wpf.Stores;
using MT.Singularity.Platform.Ribbon;
using MT.Singularity.Platform.UI.Shell.Ribbon;
using MT.Singularity.Presentation.UserInteraction;

namespace mt.proxaml.wpf.ViewModels
{
    public class FirstViewModel : RibbonViewModelBase
    {
 
        public IRibbonCommand NavigateSecondViewModelCommand { get; }
        public IRibbonCommand NavigateWeighingViewModelCommand { get; }
        public IRibbonCommand ShowUserInteractionCommand
        {
            get;
        }

        private readonly RibbonItemStore _ribbonItemStore;
        private readonly LanguageStore _languageStore;

        private RibbonItem _userInteractionKey;
        /// <summary>
        /// example to show base popup windows and langauge handling
        /// </summary>
        /// <param name="languageStore"></param>
        /// <param name="ribbonItemStore"></param>
        /// <param name="navigateToSecondViewModel"></param>
        /// <param name="userInteraction"></param>
        public FirstViewModel(LanguageStore languageStore, RibbonItemStore ribbonItemStore, INavigationService navigateToSecondViewModel, INavigationService navigateToWeighingViewModel, IUserInteraction userInteraction) : base()
        {
            NavigateSecondViewModelCommand = new NavigateCommand(navigateToSecondViewModel);
            NavigateWeighingViewModelCommand = new NavigateCommand(navigateToWeighingViewModel);
            ShowUserInteractionCommand = new ShowUserInteractionCommand(languageStore, userInteraction);
            _ribbonItemStore = ribbonItemStore;
            _languageStore = languageStore;

            AddRibbonItems();
            Text = "vom Code-Behind";
        }


        /// <summary>
        /// Fill the ribbon bar with buttons and place holder items where no button is needed
        /// </summary>
        private void AddRibbonItems()
        {
            _userInteractionKey = new RibbonItem("userinteraction")
            {
                Text = (string)_languageStore.CurrentLanguageDictionary[Globals.BUTTON_TEXT1],
                ImageSource = Globals.AddIcon,
                Command = ShowUserInteractionCommand,
                IsEnabled = true
            };

            SetRibbonItem(0, _ribbonItemStore.Printkey);
            SetRibbonItem(1, _ribbonItemStore.ZeroKey);
            SetRibbonItem(2, _ribbonItemStore.Tarekey);
            SetRibbonItem(3, _ribbonItemStore.Clearkey);
            SetRibbonItem(4, _ribbonItemStore.SwitchScalekey);
            SetRibbonItem(5, _ribbonItemStore.PlaceHolderKey);
            SetRibbonItem(6, _userInteractionKey);
            SetRibbonItem(7, _ribbonItemStore.SwitchUnitkey);
            SetRibbonItem(8, new RibbonItem("navigatesecondview")
            {
                Text = "To View 2",
                ImageSource = "Arrow_right_MTBlue",
                Command = NavigateSecondViewModelCommand,
                IsEnabled = true
            });
            SetRibbonItem(9, new RibbonItem("navigateweighing")
            {
                Text = "To Basic Weighing",
                ImageSource = "Arrow_right_MTBlue",
                Command = NavigateWeighingViewModelCommand,
                IsEnabled = true
            });
        }

        private string _text;

        public string Text
        {
            get { return _text; }
            set => SetField(ref _text, value);
        }



        public override void Dispose()
        {

            base.Dispose();
        }
    }
}
