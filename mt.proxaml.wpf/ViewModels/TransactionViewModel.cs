﻿using MT.Singularity.Composition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Identity.Client;
using mt.proxaml.application.Models;
using SQLitePCL;

namespace mt.proxaml.wpf.ViewModels
{
    /// <summary>
    /// this viewmodel is used to hold data for a single list entry and make modifications to the original source model for display purposes
    /// </summary>
    public class TransactionViewModel : SimpleViewModelBase
    {
        public Transaction SourceTransaction { get; }
        public int Id => SourceTransaction.Id;
        public string Date => SourceTransaction.TimeStamp.ToString("dd.MM.yyyy");
        public TransactionViewModel(Transaction transaction)
        {
            SourceTransaction = transaction;
        }
    }
}
