﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using mt.proxaml.wpf.Commands;
using mt.proxaml.wpf.Services;
using MT.Singularity.Composition;

namespace mt.proxaml.wpf.ViewModels
{

    /// <summary>
    /// simple example viewmodel on how to display and close a modal
    /// this can be used if more individualization is necessary that the userinteraction can provide
    /// </summary>
    public class ModalViewModel : SimpleViewModelBase
    {
        public ICommand CloseModalCommand { get; }
        public ModalViewModel(INavigationService closeModalNavigationService) 
        {
            CloseModalCommand = new CloseModalCommand(closeModalNavigationService);
        }
        public string Version => $"Version: {Assembly.GetExecutingAssembly().GetName().Version}";


    }
}
